//
//  ProjectCalloutModifier.swift
//  covid-data-handler
//
//  Created by Dawid Żubrowski on 30/01/2022.
//

import SwiftUI

struct ProjectCalloutModifier: ViewModifier {
    func body(content: Content) -> some View {
        content
            .foregroundColor(.gray)
            .font(.callout)
            .lineLimit(2)
    }
}
