//
//  JSONReaderError.swift
//  spotify-client-unit-tests
//
//  Created by Dawid Żubrowski on 11/03/2022.
//

import Foundation

enum JSONReaderError: Error {
    case invalidFileName
    case invalidDecodeModel
}

extension JSONReaderError: LocalizedError {
    public var errorDescription: String? {
        switch self {
        case .invalidFileName:
            return NSLocalizedString("Invalid file name, cannot parse JSON file.", comment: "Invalid JSON file name")
        case .invalidDecodeModel:
            return NSLocalizedString("Invalid model specified for decode.", comment: "Invalid decode model")
        }
    }
}
