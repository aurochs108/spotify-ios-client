//
//  EnvironmentValuesExtension.swift
//  covid-data-handler
//
//  Created by Dawid Żubrowski on 17/12/2021.
//

import Foundation
import SwiftUI
import Combine

private struct ViewActionsKey: EnvironmentKey {
    static let defaultValue = PassthroughSubject<ViewAction, Never>()
}

extension EnvironmentValues {
    var viewActions: PassthroughSubject<ViewAction, Never> {
        get { self[ViewActionsKey.self] }
        set { self[ViewActionsKey.self] = newValue }
    }
}
