//
//  SpotifyRequestInterceptor.swift
//  covid-data-handler
//
//  Created by Dawid Żubrowski on 10/02/2022.
//

import Alamofire
import Foundation

class ProjectRequestInterceptor: RequestInterceptor {
    private let retryLimit = 3
    private let retryDelay: TimeInterval = 1
    
    private let acceptHeader: HTTPHeader = .init(name: "Accept", value: "application/json")
    private let contentTypeHeader: HTTPHeader = .init(name: "Content-Type", value: "application/json")
    
    func retry(_ request: Request,
               for session: Session,
               dueTo error: Error,
               completion: @escaping (RetryResult) -> Void) {
        let response = request.task?.response as? HTTPURLResponse
        
        if let statusCode = response?.statusCode,
           (500...599).contains(statusCode),
           request.retryCount < retryLimit {
            completion(.retryWithDelay(retryDelay))
        } else {
            return completion(.doNotRetry)
        }
    }
    
    public func adapt(_ urlRequest: URLRequest,
                      for session: Session,
                      completion: @escaping (Result<URLRequest, Error>) -> Void) {
        if TokenUtil.isAccessTokenValid() {
            let completionResult = getResultForValidAccessToken(urlRequest: urlRequest)
            completion(completionResult)
        } else {
            let dispatchGroup = DispatchGroup()
            dispatchGroup.enter()
            
            // IN CASE THAT SOME PREVIOUS REQUEST HAS REFRESHED ACCESS TOKEN
            if TokenUtil.isAccessTokenValid() {
                let completionResult = getResultForValidAccessToken(urlRequest: urlRequest)
                completion(completionResult)
            } else {
                AuthManager.shared.getRefreshedAccessToken { [weak self] result in
                    guard let strongSelf = self else { return }
                    switch result {
                    case .success(let refreshTokenModel):
                        TokenUtil.saveAccessToken(authToken: refreshTokenModel.accessToken,
                                                  expiresInSeconds: refreshTokenModel.expiresIn)
                        let accessToken = refreshTokenModel.accessToken
                        let urlRequest = strongSelf.setUrlRequest(accessToken: accessToken,
                                                                  urlRequest: urlRequest)
                        completion(.success(urlRequest))
                    case .failure(let error):
                        completion(.failure(error))
                    }
                }
            }
            dispatchGroup.leave()
            dispatchGroup.wait()
        }
    }
    
    private func getResultForValidAccessToken(urlRequest: URLRequest) -> Result<URLRequest, Error> {
        do {
            let accessToken = try TokenUtil.getAuthToken()
            let urlRequest = setUrlRequest(accessToken: accessToken,
                                           urlRequest: urlRequest)
            return .success(urlRequest)
        } catch let error {
            return .failure(error)
        }
    }
    
    private func setUrlRequest(accessToken: String,
                               urlRequest: URLRequest) -> URLRequest {
        var urlRequest = urlRequest
        let authorizationHeader: HTTPHeader = .init(name: "Authorization", value: "Bearer \(accessToken)")
        urlRequest.headers = .init([acceptHeader,
                                    contentTypeHeader,
                                    authorizationHeader])
        return urlRequest
    }
}
