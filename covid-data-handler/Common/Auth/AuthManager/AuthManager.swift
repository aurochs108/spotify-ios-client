//
//  AuthManager.swift
//  covid-data-handler
//
//  Created by Dawid Żubrowski on 20/12/2021.
//

import Foundation
import Alamofire
import Combine
import SwiftKeychainWrapper

class AuthManager {
    static let shared = AuthManager()
    let appAuthAction = CurrentValueSubject<AppAuthAction, Never>(.loading)
    private static let encodedCredentials = "OGMzZWEwYzQyYTZjNGIzZDkzMzlmNDM0NDI2MzA0NDc6Y2ZhYWNjNjAxYWMxNGY0M2FkOWRmNjYzNWM5NTNkMTc="
    private let authHeader: HTTPHeaders = ["Authorization": "Basic \(AuthManager.encodedCredentials)"]
    private var cancelBag = Set<AnyCancellable>()
    
    private init() {
        bindRefreshAccessTokenAction()
        setAppStatus()
    }
    
    func logout() {
        appAuthAction.send(.loading)
        TokenUtil.deleteAllTokens()
        WebKitHelper.clearAllCookies()
        appAuthAction.send(.showLoginPage)
    }
    
    func getRefreshedAccessToken(completionHandler: @escaping (Result<RefreshTokenModel, AuthError>) -> Void) {
        guard let refreshToken = KeychainWrapper
                .standard
                .string(forKey: AuthManager.KEYCHAIN_STATICS.REFRESH_TOKEN_KEY) else {
                    completionHandler(.failure(.refreshTokenNotExist))
                    return
                }
        
        let parameters = RefreshTokenParameters(refreshToken: refreshToken)
        
        AF.request(ProjectData.Auth.TOKEN,
                   method: .post,
                   parameters: parameters,
                   headers: authHeader)
            .validate(statusCode: 200..<300)
            .responseData { (response: AFDataResponse<Data>) in
                switch response.result {
                case .success(let data):
                    do {
                        let refreshTokenModel = try JSONDecoder().decode(RefreshTokenModel.self, from: data)
                        completionHandler(.success(refreshTokenModel))
                    } catch(let error) {
                        print(error)
                        completionHandler(.failure(.refreshNotPossible))
                    }
                case .failure(let error):
                    print(error)
                    completionHandler(.failure(.refreshNotPossible))
                }
            }
    }
    
    func getAccessToken(code: String,
                        completion: @escaping (Result<TokenModel, Error>) -> Void) {
        let parameters = AccessTokenParameters(code: code)
        
        AF.request(ProjectData.Auth.TOKEN,
                   method: .post,
                   parameters: parameters,
                   headers: authHeader)
            .validate(statusCode: 200..<300)
            .responseData { (response: AFDataResponse<Data>) in
                switch response.result {
                case .success(let data):
                    do {
                        let tokenModel = try JSONDecoder().decode(TokenModel.self, from: data)
                        completion(.success(tokenModel))
                    } catch(let error) {
                        completion(.failure(error))
                    }
                case .failure(let error):
                    completion(.failure(error))
                }
            }
    }
    
    private func setAppStatus() {
        guard TokenUtil.isRefreshTokenExist() else {
            appAuthAction.send(.showLoginPage)
            return
        }
        
        if TokenUtil.isAccessTokenValid() {
            appAuthAction.send(.showApp)
            return
        }
        appAuthAction.send(.refreshAccessToken)
    }
    
    private func bindRefreshAccessTokenAction() {
        self.appAuthAction
            .filter { $0 == .refreshAccessToken }
            .subscribe(on: RunLoop.main)
            .sink { action in
                self.getRefreshedAccessToken(completionHandler: { result in
                    switch result {
                    case .success(let refreshTokenModel):
                        TokenUtil.saveAccessToken(authToken: refreshTokenModel.accessToken,
                                                  expiresInSeconds: refreshTokenModel.expiresIn)
                        self.appAuthAction.send(.showApp)
                    case .failure(let error):
                        print(error)
                        self.appAuthAction.send(.showLoginPage)
                    }
                })
            }
            .store(in: &cancelBag)
    }
}
