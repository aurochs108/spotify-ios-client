//
//  TokenModel.swift
//  covid-data-handler
//
//  Created by Dawid Żubrowski on 20/12/2021.
//

import Foundation

struct TokenModel: Codable {
    var accessToken: String
    var tokenType: String
    var scope: String
    var expiresIn: Int
    var refreshToken: String
    
    private enum CodingKeys : String, CodingKey {
        case accessToken = "access_token"
        case tokenType = "token_type"
        case scope
        case expiresIn = "expires_in"
        case refreshToken = "refresh_token"
    }
    
    mutating func refreshToken(model: RefreshTokenModel) {
        self.accessToken = model.accessToken
        self.tokenType = model.tokenType
        self.scope = model.scope
        self.expiresIn = model.expiresIn
    }
}

struct RefreshTokenModel: Codable {
    let accessToken: String
    let tokenType: String
    let scope: String
    let expiresIn: Int
    
    private enum CodingKeys : String, CodingKey {
        case accessToken = "access_token"
        case tokenType = "token_type"
        case scope
        case expiresIn = "expires_in"
    }
}

struct AccessTokenParameters: Codable {
    let code: String
    var redirectUri = ProjectLinks.Login.CALLBACK_URI
    var grantType = "authorization_code"
    
    private enum CodingKeys : String, CodingKey {
        case code
        case redirectUri = "redirect_uri"
        case grantType = "grant_type"
    }
}

struct RefreshTokenParameters: Codable {
    var grantType = "refresh_token"
    let refreshToken: String
    
    private enum CodingKeys : String, CodingKey {
        case grantType = "grant_type"
        case refreshToken = "refresh_token"
    }
}
