//
//  LazyNavigationView.swift
//  covid-data-handler
//
//  Created by Dawid Żubrowski on 10/01/2022.
//

import SwiftUI

struct NavigationLazyView<Content: View>: View {
    let build: () -> Content
    init(_ build: @autoclosure @escaping () -> Content) {
        self.build = build
    }
    var body: Content {
        build()
    }
}
