//
//  GradientBackgroundView.swift
//  covid-data-handler
//
//  Created by Dawid Żubrowski on 23/01/2022.
//

import SwiftUI

struct GradientBackgroundView: View {
    @Binding var upperColor: UIColor
    
    var body: some View {
        LinearGradient(gradient: Gradient(colors: [Color(upperColor), .black, .black]),
                       startPoint: .top,
                       endPoint: .bottom)
            .ignoresSafeArea()
    }
}

struct GradientBackgroundView_Previews: PreviewProvider {
    static var previews: some View {
        GradientBackgroundView(upperColor: .constant(.blue))
    }
}
