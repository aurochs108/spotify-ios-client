//
//  SplashScreenViewModel.swift
//  covid-data-handler
//
//  Created by Dawid Żubrowski on 26/12/2021.
//

import Foundation
import SwiftUI

class MasterViewModel: BaseViewModel {
    private let authManager = AuthManager.shared
    @Published var appState: AppAuthAction = .loading
    
    override init() {
        super.init()
        bindOutput()
    }
    
    private func bindOutput() {
        authManager.appAuthAction
            .receive(on: RunLoop.main)
            .sink { [weak self] authAction in
                self?.appState = authAction
            }
            .store(in: &cancelBag)
    }

}
