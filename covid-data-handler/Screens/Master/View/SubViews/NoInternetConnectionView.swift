//
//  NoInternetConnectionView.swift
//  covid-data-handler
//
//  Created by Dawid Żubrowski on 12/02/2022.
//

import SwiftUI

struct NoInternetConnectionView: View {
    var body: some View {
        ZStack {
            Color("MainColor")
                .ignoresSafeArea()
            VStack {
                Text("No internet connection")
                    .font(.title.weight(.medium))
                    .foregroundColor(.white)
                RoundedButton(buttonTitle: "Retry") {
                    retry()
                }
            }
            
        }
    }
    
    private func retry() {
        //TODO
        print("retry")
    }
}

struct NoInternetConnectionView_Previews: PreviewProvider {
    static var previews: some View {
        NoInternetConnectionView()
    }
}
