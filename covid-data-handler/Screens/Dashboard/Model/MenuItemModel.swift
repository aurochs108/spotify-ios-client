//
//  MenuItemModel.swift
//  covid-data-handler
//
//  Created by Dawid Żubrowski on 12/12/2021.
//

import Foundation

struct MenuItemModel: Hashable {
    let title: String
    let position: Int
}
