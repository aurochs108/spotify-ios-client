//
//  LoginError.swift
//  covid-data-handler
//
//  Created by Dawid Żubrowski on 12/02/2022.
//

import Foundation

enum LoginError: Error {
    case loginCodeNotRetrieved
}
