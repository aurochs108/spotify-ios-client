//
//  PlaylistCellView.swift
//  covid-data-handler
//
//  Created by Dawid Żubrowski on 03/02/2022.
//

import SwiftUI

struct PlaylistTrackCell: View {
    let image: URL?
    let trackName: String
    let artistsNames: String
    
    var body: some View {
        HStack {
            StandardizedAsyncImage(imageURL: image)
                .frame(width: 60, height: 60)
            VStack(alignment: .leading) {
                Text(trackName)
                    .foregroundColor(.white)
                Text(artistsNames)
                    .foregroundColor(.gray)
            }
            Spacer()
        }
    }
}
