//
//  PlaylistViewModel.swift
//  covid-data-handler
//
//  Created by Dawid Żubrowski on 10/01/2022.
//

import Foundation
import CoreData
import SwiftUI

class PlaylistViewModel: TracksListViewModel {
    private lazy var playlistTracksNetworkRepository = PlaylistTracksNetworkRepository()
    private var playlistItemDbRepository: PlaylistItemDbRepository!
    
    private let playlistId: String
    var playlistsItemsModels: [PlaylistTracksNetworkModel.Track] = []
    let playlistModel: PlaylistModelProtocol
    
    @Published var filteredPlaylists: [PlaylistTracksNetworkModel.Track] = []
    
    init(playlistId: String,
         playlistModel: PlaylistModelProtocol) {
        self.playlistId = playlistId
        self.playlistModel = playlistModel
        super.init()
        self.playlistItemDbRepository = .init(context: self.context)
        getPlaylistItems(playlistId)
        
    }
    
    func filterPlaylistItems(input: String) {
        self.filteredPlaylists = filterItems(input: input,
                                             filteringTarget: self.playlistsItemsModels)
    }
    
    private func getPlaylistItems(_ playlistId: String) {
        let getPlaylistItems = playlistTracksNetworkRepository.getPlaylistItems(playlistId: playlistId)
        
        guard Connectivity.isConnectedToInternet else {
            viewState = .noInternet
            do {
                let playlistItem = try playlistItemDbRepository.getObject(playlistId: playlistId)
                let mapper = PlaylistItemDbToNetworkMapper()
                let mappedPlaylistsItems = mapper.map(input: playlistItem).items
                self.playlistsItemsModels = mappedPlaylistsItems.map({ $0.track })
                viewState = .show
                //show some info about no internet
                return
            } catch {
                viewState = .error
                return
            }
        }
    
        getPlaylistItems
            .sink { completion in
                print(completion)
            } receiveValue: { [weak self] responseModel in
                guard let strongSelf = self,
                      responseModel.responseCode == StatusCodes.code200OK else { return }
                strongSelf.playlistItemDbRepository.saveOrUpdate(model: responseModel.model!,
                                                                 playlistId: strongSelf.playlistId)
                let items = responseModel.model?.items ?? []
                strongSelf.playlistsItemsModels = items.map({ $0.track })
                strongSelf.filteredPlaylists = strongSelf.playlistsItemsModels
                strongSelf.viewState = .show
            }
            .store(in: &cancelBag)
    }
}
