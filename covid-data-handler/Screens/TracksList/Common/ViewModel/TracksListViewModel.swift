//
//  TracksListViewModel.swift
//  covid-data-handler
//
//  Created by Dawid Żubrowski on 08/02/2022.
//

import Foundation

class TracksListViewModel: BaseViewModel {
    
    func filterItems<ITEM: TrackItem>(input: String,
                                      filteringTarget: [ITEM]) -> [ITEM] {
        guard !input.isEmpty else { return filteringTarget }
        
        let inputLowerCase = input.lowercased()
        return filteringTarget.filter { item in
            item.artistsNames.lowercased().contains(inputLowerCase) ||
            item.name.lowercased().contains(inputLowerCase)
        }
    }
}
