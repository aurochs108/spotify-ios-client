//
//  TracksListActionButtonsView.swift
//  covid-data-handler
//
//  Created by Dawid Żubrowski on 03/02/2022.
//

import SwiftUI

struct TracksListActionsButtons: View {
    
    var body: some View {
        VStack(alignment: .leading) {
            HStack(spacing: 25) {
                TracksListActionButton(imageName: "EmptyHeart",
                             imageColor: .green) {
                    print("Heart tapped")
                }
                             .frame(width: 30, height: 30)
                
                TracksListActionButton(imageName: "Download",
                             imageColor: .gray) {
                    print("Download tapped")
                }
                             .frame(width: 30, height: 30)
                
                TracksListActionButton(imageName: "ThreeDots",
                             imageColor: .gray) {
                    print("Three dots tapped")
                }
                             .frame(width: 30, height: 30)
            }
        }
    }
}
