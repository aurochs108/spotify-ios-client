//
//  PlaylistCell.swift
//  covid-data-handler
//
//  Created by Dawid Żubrowski on 09/01/2022.
//

import SwiftUI

struct PlaylistCell: View {
    let title: String
    let playlistImageUrl: URL?
    let cellHeight: CGFloat
    
    var body: some View {
        HStack {
            StandardizedAsyncImage(imageURL: playlistImageUrl,
                                   contentMode: .fill)
                .frame(width: 50, height: 50)
            VStack(alignment: .leading) {
                Text(title)
                    .foregroundColor(.white)
                    .font(.headline.bold())
                    .lineLimit(.zero)
                Text("todo likes")
                    .foregroundColor(.white)
            }
            Spacer()
            Image(systemName: "chevron.right")
                .foregroundColor(.white)
        }
        .frame(height: cellHeight)
    }
}

struct PlaylistCell_Previews: PreviewProvider {
    static var previews: some View {
        PlaylistCell(title: "test",
                     playlistImageUrl: PreviewStatics.randomImageURL,
                     cellHeight: 60)
    }
}
