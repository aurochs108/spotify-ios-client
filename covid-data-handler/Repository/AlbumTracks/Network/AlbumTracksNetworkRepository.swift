//
//  AlbumTracksNetworkRepository.swift
//  covid-data-handler
//
//  Created by Dawid Żubrowski on 01/02/2022.
//

import Foundation
import Combine
import Alamofire

class AlbumTracksNetworkRepository: NetworkRepository,
                                    AlbumTracksRepositoryProtocol {
    func getAlbumTracks(albumId: String) -> AnyPublisher<ResponseModel<AlbumTrackNetworkModel>, Error> {
        let url = String(format: ProjectData.Albums.GET_ALBUM_TRACKS, albumId)
        return super.getModel(url: url)
    }
}
