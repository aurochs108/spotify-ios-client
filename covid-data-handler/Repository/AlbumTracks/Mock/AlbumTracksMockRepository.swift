//
//  AlbumTracksMockRepository.swift
//  spotify-client-unit-tests
//
//  Created by Dawid Żubrowski on 11/03/2022.
//

import Foundation
import Combine

class AlbumTracksMockRepository: MockRepository,
                                    AlbumTracksRepositoryProtocol {
    func getAlbumTracks(albumId: String) -> AnyPublisher<ResponseModel<AlbumTrackNetworkModel>, Error> {
        super.getMockDataRequest(fileName: "ExampleAlbumTracks")
    }
}
