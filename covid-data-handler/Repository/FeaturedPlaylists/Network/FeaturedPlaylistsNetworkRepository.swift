//
//  FeaturedPlaylistsNetworkReopository.swift
//  covid-data-handler
//
//  Created by Dawid Żubrowski on 30/01/2022.
//

import Foundation
import Combine
import Alamofire

class FeaturedPlaylistsNetworkRepository: NetworkRepository,
                                          FeaturedPlaylistsRepositoryProtocol {
    func getFeaturedPlaylists(parameters: FeaturedPlaylistsNetworkParameters) -> AnyPublisher<ResponseModel<FeaturedPlaylistsNetworkModel>, Error> {
        super.getModel(url: ProjectData.Playlists.GET_FEATURED_PLAYLISTS,
                       parameters: parameters)
    }
}
