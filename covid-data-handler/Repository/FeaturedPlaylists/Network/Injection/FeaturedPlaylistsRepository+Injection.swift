//
//  FeaturedPlaylists+Injection.swift
//  covid-data-handler
//
//  Created by Dawid Żubrowski on 15/02/2022.
//

import Resolver

extension Resolver {
    public static func registerFeaturedPlaylistsRepository() {
        register { FeaturedPlaylistsNetworkRepository() }
        .implements(NetworkRepository.self)
    }
}
