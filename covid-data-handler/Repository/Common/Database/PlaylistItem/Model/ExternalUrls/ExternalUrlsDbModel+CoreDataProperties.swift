//
//  ExternalUrlsDbModel+CoreDataProperties.swift
//  covid-data-handler
//
//  Created by Dawid Żubrowski on 17/01/2022.
//
//

import Foundation
import CoreData


extension ExternalUrlsDbModel {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<ExternalUrlsDbModel> {
        return NSFetchRequest<ExternalUrlsDbModel>(entityName: "ExternalUrlsDbModel")
    }

    @NSManaged public var spotify: String?
    @NSManaged public var addedByOrigin: AddedByDbModel?
    @NSManaged public var albumOrigin: AlbumDbModel?
    @NSManaged public var trackOrigin: TrackDbModel?

}

extension ExternalUrlsDbModel : Identifiable {

}
