//
//  PlaylistModelProtocol.swift
//  covid-data-handler
//
//  Created by Dawid Żubrowski on 02/02/2022.
//

import Foundation

<<<<<<< HEAD:covid-data-handler/Repository/Common/Models/Protocols/PlaylistModelProtocol.swift
protocol PlaylistModelProtocol {
=======
protocol PlaylistModel {
>>>>>>> develop:covid-data-handler/Repository/Common/Models/Protocols/PlaylistProtocol.swift
    var description: String? { get }
    var mainImage: URL? { get }
    var name: String { get }
    var artistsNames: String { get }
    var type: MusicType { get }
}
