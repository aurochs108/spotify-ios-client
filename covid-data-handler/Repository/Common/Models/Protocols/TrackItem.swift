//
//  TrackItem.swift
//  covid-data-handler
//
//  Created by Dawid Żubrowski on 08/02/2022.
//

protocol TrackItem {
    var artistsNames: String { get }
    var name: String { get }
}
