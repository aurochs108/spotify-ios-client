//
//  ExternalUrlsNetworkModel.swift
//  covid-data-handler
//
//  Created by Dawid Żubrowski on 06/01/2022.
//

import Foundation

struct ExternalUrlsNetworkModel: Codable,
                                 Hashable {
    let spotify: String
}
