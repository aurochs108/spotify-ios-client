//
//  OwnerNetworkModel.swift
//  covid-data-handler
//
//  Created by Dawid Żubrowski on 30/01/2022.
//

import Foundation

struct OwnerNetworkModel: Codable {
    let displayName: String
    let externalUrls: ExternalUrlsNetworkModel
    let href: String
    let id, type, uri: String
}
