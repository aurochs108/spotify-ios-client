//
//  PlaylistItemNetworkToDBMapper.swift
//  covid-data-handler
//
//  Created by Dawid Żubrowski on 17/01/2022.
//

import Foundation
import CoreData

class PlaylistItemNetworkToDbMapper {
    func map(input: PlaylistTracksNetworkModel,
             context: NSManagedObjectContext) -> PlaylistItemDbModel {
        let playlistItem = PlaylistItemDbModel(context: context)
        playlistItem.href = input.href
        let itemsArray = input.items.map { mapItem(input: $0,
                                                   context: context) }
        playlistItem.items = NSSet(array: itemsArray)
        playlistItem.limit = Int16(input.limit)
        playlistItem.next = input.next
        playlistItem.offset = Int16(input.offset)
        playlistItem.previous = input.previous
        playlistItem.total = Int16(input.total)
        return playlistItem
    }
    
    private func mapItem(input: PlaylistTracksNetworkModel.Item,
                         context: NSManagedObjectContext) -> ItemDbModel {
        let item = ItemDbModel(context: context)
        item.addedAt = input.addedAt
        item.addedBy = mapAddedBy(input: input.addedBy,
                                  context: context)
        item.isLocal = input.isLocal
        item.primaryColor = input.primaryColor
        item.track = mapTrack(input: input.track,
                              context: context)
        item.videoThumbnail = mapVideoThumbnail(input: input.videoThumbnail,
                                                context: context)
        return item
    }
    
    private func mapExternalIDS(input: PlaylistTracksNetworkModel.ExternalIDS,
                                context: NSManagedObjectContext) -> ExternalIDSDbModel {
        let externalIDS = ExternalIDSDbModel(context: context)
        externalIDS.isrc = input.isrc
        return externalIDS
    }
    
    private func mapImageEntity(input: ImageNetworkModel,
                                context: NSManagedObjectContext) -> ImageDbModel {
        let imageEntity = ImageDbModel(context: context)
        if let height = input.height {
            imageEntity.height = Int16(height)
        }
        imageEntity.url = input.url.absoluteString
        if let width = input.width {
            imageEntity.width = Int16(width)
        }
        return imageEntity
    }
    
    private func mapAlbum(input: AlbumNetworkModel,
                          context: NSManagedObjectContext) -> AlbumDbModel {
        let album = AlbumDbModel(context: context)
        album.albumType = input.albumType.rawValue
        let addedByArray = input.artists.map { mapAddedBy(input: $0, context: context) }
        album.artists = NSSet(array: addedByArray)
        album.avaliableMarkets = input.availableMarkets
        album.externalUrls = mapExternalUrls(input: input.externalUrls,
                                             context: context)
        album.href = input.href
        album.id = input.id
        let imagesArray = input.images.map { mapImageEntity(input: $0,
                                                            context: context) }
        album.images = NSSet(array: imagesArray)
        album.name = input.name
        album.releaseDate = input.releaseDate
        album.releaseDatePrecision = input.releaseDatePrecision.rawValue
        album.totalTracks = Int16(input.totalTracks)
        album.type = input.type.rawValue
        album.uri = input.uri
        return album
    }
    
    private func mapTrack(input: PlaylistTracksNetworkModel.Track,
                          context: NSManagedObjectContext) -> TrackDbModel {
        let track = TrackDbModel(context: context)
        track.album = mapAlbum(input: input.album,
                               context: context)
        let artistsArray = input.artists.map { mapAddedBy(input: $0, context: context) }
        track.artists = NSSet(array: artistsArray)
        track.avaliableMarkets = input.availableMarkets
        track.discNumber = Int16(input.discNumber)
        if let durationMS = input.durationMS {
            track.durationMS = Int16(durationMS)
        }
        track.episode = input.episode
        track.explicit = input.explicit
        if let externalIDS = input.externalIDS {
            track.externalIDS = mapExternalIDS(input: externalIDS, context: context)
        }
        track.externalUrls = mapExternalUrls(input: input.externalUrls, context: context)
        track.href = input.href
        track.id = input.id
        track.isLocal = input.isLocal
        track.name = input.name
        track.popularity = Int16(input.popularity)
        track.previewURL = input.previewURL
        track.track = input.track
        track.trackNumber = Int16(input.trackNumber)
        track.type = input.type.rawValue
        track.uri = input.uri
        return track
    }
    
    private func mapAddedBy(input: AddedByNetworkModel,
                            context: NSManagedObjectContext) -> AddedByDbModel {
        let addedBy = AddedByDbModel(context: context)
        addedBy.externalUrls = mapExternalUrls(input: input.externalUrls,
                                               context: context)
        addedBy.href = input.href
        addedBy.id = input.id
        addedBy.type = input.type.rawValue
        addedBy.uri = input.uri
        addedBy.name = input.name
        return addedBy
    }
    
    private func mapExternalUrls(input: ExternalUrlsNetworkModel,
                                 context: NSManagedObjectContext) -> ExternalUrlsDbModel {
        let externalUrls = ExternalUrlsDbModel(context: context)
        externalUrls.spotify = input.spotify
        return externalUrls
    }
    
    private func mapVideoThumbnail(input: PlaylistTracksNetworkModel.VideoThumbnail,
                                   context: NSManagedObjectContext) -> VideoThumbnailDbModel {
        let videoThumbnail = VideoThumbnailDbModel(context: context)
        videoThumbnail.url = input.url?.absoluteString
        return videoThumbnail
    }
}
