//
//  NewReleasesRepositoryProtocol.swift
//  covid-data-handler
//
//  Created by Dawid Żubrowski on 02/03/2022.
//

import Foundation
import Combine
import Alamofire

protocol NewReleasesRepositoryProtocol {
    func getNewReleases() -> AnyPublisher<ResponseModel<NewReleaseNetworkModel>, Error>
}
