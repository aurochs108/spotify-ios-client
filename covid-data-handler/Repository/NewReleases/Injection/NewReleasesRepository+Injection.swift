//
//  NewReleasesRepository+Injection.swift
//  covid-data-handler
//
//  Created by Dawid Żubrowski on 02/03/2022.
//

import Resolver

extension Resolver {
    static func registerNewReleasesRepository() {
        register { NewReleasesNetworkRepository() }
        .implements(NewReleasesRepositoryProtocol.self)
    }
}
