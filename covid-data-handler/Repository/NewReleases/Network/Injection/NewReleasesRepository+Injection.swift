//
//  NewReleasesRepository+Injection.swift
//  covid-data-handler
//
//  Created by Dawid Żubrowski on 14/02/2022.
//

import Resolver

extension Resolver {
    public static func registerNewReleasesRepository() {
        register { NewReleasesNetworkRepository() }
        .implements(NetworkRepository.self)
    }
}
