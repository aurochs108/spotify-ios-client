//
//  ProfileNetworkModel.swift
//  covid-data-handler
//
//  Created by Dawid Żubrowski on 29/12/2021.
//

import Foundation

struct ProfileNetworkModel: Codable {
    let country: String
    let displayName: String?
    let email: String
    let explicitContent: ProfileExplicitContent
    let externalUrls: ExternalUrlsNetworkModel
    let followers: ProfileFolowers
    let href: String
    let id: String
    let images: [ImageNetworkModel]
    let product: String
    let type: String
    let uri: String
    
    // MARK: - ExplicitContent
    struct ProfileExplicitContent: Codable {
        let filterEnabled: Bool
        let filterLocked: Bool
    }
    
    // MARK: - Followers
    struct ProfileFolowers: Codable {
        let href: String?
        let total: Int
    }
}


