//
//  ProfileRepositoryProtocol.swift
//  covid-data-handler
//
//  Created by Dawid Żubrowski on 02/03/2022.
//

import Foundation
import Combine
import Alamofire

protocol ProfileRepositoryProtocol {
    func getCurrentUserProfile() -> AnyPublisher<ResponseModel<ProfileNetworkModel>, Error>
}
