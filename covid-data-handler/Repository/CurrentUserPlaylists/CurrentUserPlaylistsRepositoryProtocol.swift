//
//  CurrentUserPlaylistsRepositoryProtocol.swift
//  covid-data-handler
//
//  Created by Dawid Żubrowski on 02/03/2022.
//

import Foundation
import Combine
import Alamofire

protocol PlaylistsRepositoryProtocol {
    func getCurrentUserPlaylists() -> AnyPublisher<ResponseModel<PlaylistsNetworkModel>, Error>
}
