//
//  PlaylistNetworkRepository+Injection.swift
//  covid-data-handler
//
//  Created by Dawid Żubrowski on 06/03/2022.
//

import Resolver

extension Resolver {
    static func registerPlaylistsRepository() {
        register { PlaylistsNetworkRepository() }
        .implements(PlaylistsRepositoryProtocol.self)
    }
}
