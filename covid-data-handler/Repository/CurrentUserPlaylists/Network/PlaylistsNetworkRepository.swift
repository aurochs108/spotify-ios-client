//
//  PlaylistsNetworkRepository.swift
//  covid-data-handler
//
//  Created by Dawid Żubrowski on 06/01/2022.
//

import Foundation
import Combine
import Alamofire

class PlaylistsNetworkRepository: NetworkRepository,
                                  PlaylistsRepositoryProtocol {
    func getCurrentUserPlaylists() -> AnyPublisher<ResponseModel<PlaylistsNetworkModel>, Error> {
        super.getModel(url: ProjectData.Playlists.GET_CURRENT_USER_PLAYLISTS)
    }
}
