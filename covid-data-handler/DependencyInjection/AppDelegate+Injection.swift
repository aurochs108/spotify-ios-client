//
//  AppDelegate+Injection.swift
//  covid-data-handler
//
//  Created by Dawid Żubrowski on 02/03/2022.
//

import Resolver

extension Resolver: ResolverRegistering {
    public static func registerAllServices() {
        registerNewReleasesRepository()
        registerFeaturedPlaylistsRepository()
        registerProfileNetworkRepository()
        registerAlbumTracksNetworkRepository()
        registerPlaylistsRepository()
    }
}
